# Conteneuriser une application donnée

Ici, une application codé en python nous est donnée. On doit la conteneuriser.

## Installation

```
docker-compose up -d
```

## Webserver

On crée une image docker qui permet de démarrer l'application.

Le `Dockerfile` :

```
FROM alpine:latest

RUN apk update && apk add python3 && apk add py-pip

COPY ./app/requirements /app/requirements

WORKDIR /app

RUN pip install -r requirements

COPY ./app /app

ENTRYPOINT [ "python" ]

CMD ["app.py"]
```

## Nginx

Même principe que l'exercices d'avant.

## Database (Redis)

L'application a besoin d'une base de données pour fonctionner. Pour cela dans le docker-compose, on lui crée un service. Ce service (Redis) a un aliase `db` et son port `6379` exposé.
