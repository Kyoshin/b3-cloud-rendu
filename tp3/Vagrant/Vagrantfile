# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
  
  (1..3).each do |host|
    config.vm.define "vagrant#{host}" do |vagrant|
      vagrant.vm.network "private_network", ip: "192.168.33.1#{host}"
      vagrant.vm.hostname = "node#{host}.b3"
      vagrant.vm.synced_folder "./data", "/data", type: "rsync"

      vagrant.vm.provider :virtualbox do |vb|
        vb.name = "node-#{host}"
        vb.memory = "1024"

        # Set disk path
        vb_machine_folder = "/Kyoshin/VirtualBox VMs"
        second_disk = File.join(vb_machine_folder, vb.name, 'secondDisk.vdi')

        # Create and attach disk
        unless File.exist?(second_disk)
          vb.customize ['createhd', '--filename', second_disk, '--variant', 'Standard', '--size', 5 * 1024]
          vb.customize ['storageattach', :id,  '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', second_disk]
        end

        # Create and attach disk
        if(vb.name == "node-3")
          third_disk = File.join(vb_machine_folder, vb.name, 'thirdDisk.vdi')
          unless File.exist?(third_disk)
            vb.customize ['storagectl', :id, '--name', 'SATA', '--add', 'sata', '--controller', 'IntelAhci', '--portcount', 1]
            vb.customize ['createhd', '--filename', third_disk, '--variant', 'Standard', '--size', 5 * 1024]
            vb.customize ['storageattach', :id,  '--storagectl', 'SATA', '--port', 0, '--device', 0, '--type', 'hdd', '--medium', third_disk]
          end
        end
      end
    end
  end

  config.vm.provision "shell", inline: <<-SHELL
    yum -y update
    yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    yum install -y docker-ce docker-ce-cli containerd.io lvm2
	curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
	chmod +x /usr/local/bin/docker-compose
	systemctl start docker
    systemctl enable docker
    setenforce 0
    cp /data/hosts /etc/hosts
    cp /data/config /etc/selinux/config
  SHELL
end
