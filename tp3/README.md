# TP 3 - Servervice Orchestration & Cloud-native environment
Auteur : Jonathan DINH<br/>
Classe : B3B<br/>
Date de création : 23/02/2020<br/>

## I. Lab setup

Utilisation de Vagrant pour monter le Lab :

```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
  
  (1..3).each do |host|
    config.vm.define "vagrant#{host}" do |vagrant|
      vagrant.vm.network "private_network", ip: "192.168.33.1#{host}"
      vagrant.vm.hostname = "node#{host}.b3"
      vagrant.vm.synced_folder "./data", "/data", type: "rsync"

      vagrant.vm.provider :virtualbox do |vb|
        vb.name = "node-#{host}"
        vb.memory = "1024"

        # Set disk path
        vb_machine_folder = "/Kyoshin/VirtualBox VMs"
        second_disk = File.join(vb_machine_folder, vb.name, 'secondDisk.vdi')

        # Create and attach disk
        unless File.exist?(second_disk)
          vb.customize ['createhd', '--filename', second_disk, '--variant', 'Standard', '--size', 5 * 1024]
          vb.customize ['storageattach', :id,  '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', second_disk]
        end

        # Create and attach disk
        if(vb.name == "node-3")
          third_disk = File.join(vb_machine_folder, vb.name, 'thirdDisk.vdi')
          unless File.exist?(third_disk)
            vb.customize ['storagectl', :id, '--name', 'SATA', '--add', 'sata', '--controller', 'IntelAhci', '--portcount', 1]
            vb.customize ['createhd', '--filename', third_disk, '--variant', 'Standard', '--size', 5 * 1024]
            vb.customize ['storageattach', :id,  '--storagectl', 'SATA', '--port', 0, '--device', 0, '--type', 'hdd', '--medium', third_disk]
          end
        end
      end
    end
  end

  config.vm.provision "shell", inline: <<-SHELL
    yum -y update
    yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    yum install -y docker-ce docker-ce-cli containerd.io
    setenforce 0
    cp /data/hosts /etc/hosts
    cp /data/config /etc/selinux/config
  SHELL
end
```

## II. Mise en place de Docker Swarm

### 1. Setup
🌞 Utilisez des commandes `docker` afin de créer votre cluster Swarm

-   `docker swarm init` pour créer le swarm
    -   utilisez le `--advertise-addr` pour prciser l'adresse dans le LAN
-   `docker swarm join` pour qu'un noeud rejoigne un swarm existant
-   **vos 3 machines doivent être des _managers Swarm_**

Sur `node1` :
```bash
[vagrant@node1 ~]$ sudo docker swarm init --advertise-addr 192.168.33.11
Swarm initialized: current node (xbjdan7doim8bsklemgjjw8ag) is now a manager.

[...]

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

[vagrant@node1 ~]$ sudo docker swarm join-token manager
To add a manager to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-0piok2ihvtrhkvbhvjky6awqi51qg14jxwa2z83tw7cw0sqvxe-5htiqkqsnukrc7tt93tog3yuk 192.168.33.11:2377
```

Sur `node2` :
```bash
[vagrant@node2 ~]$ sudo docker swarm join --token SWMTKN-1-0piok2ihvtrhkvbhvjky6awqi51qg14jxwa2z83tw7cw0sqvxe-5htiqkqsnukrc7tt93tog3yuk 192.168.33.11:2377
This node joined a swarm as a manager.
```

Sur `node3` :
```bash
[vagrant@node3 ~]$ sudo docker swarm join --token SWMTKN-1-0piok2ihvtrhkvbhvjky6awqi51qg14jxwa2z83tw7cw0sqvxe-5htiqkqsnukrc7tt93tog3yuk 192.168.33.11:2377
This node joined a swarm as a manager.
```

Vérification que tous les noeuds sont dans le cluster :
```bash
[vagrant@node3 ~]$ sudo docker node ls
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
xbjdan7doim8bsklemgjjw8ag     node1.b3            Ready               Active              Leader              19.03.6
vqp0dnhheulh5ngxaikvmb743     node2.b3            Ready               Active              Reachable           19.03.6
f80nzala9kg6w5ak0anhng3rd *   node3.b3            Ready               Active              Reachable           19.03.6
```

### 2. Une première application orchestrée
🌞 "swarmiser" l'application Python du TP1

-   récuprez le `docker-compose.yml` que vous aviez écrit et le déployer dans le Swarm
    -   **l'image doit être présente sur tous les noeuds du cluster**
        -   vous devez donc la build sur tous les noeuds du cluster
            -   `docker build` du Dockerfile **sur les 3 machines**
    -   utiliser un partage de ports pour accéder  l'application
        -   pour rappel, elle écoute sur le port 8888/TCP

Sur les nodes ont build les images de l'app python :
```bash
[vagrant@node1 ~]$ sudo docker build -t "python_app:latest" /data/python_app/webserver/
Sending build context to Docker daemon  6.656kB
Step 1/8 : FROM alpine:latest
latest: Pulling from library/alpine
c9b1b535fdd9: Pull complete

[...]

Step 6/8 : COPY ./app /app
 ---> 870281f84b5c
Step 7/8 : ENTRYPOINT [ "python" ]
 ---> Running in be693f5137ee
Removing intermediate container be693f5137ee
 ---> 61ba3babdad4
Step 8/8 : CMD ["app.py"]
 ---> Running in 517de0f7dedf
Removing intermediate container 517de0f7dedf
 ---> 3eba28a1f104
Successfully built 3eba28a1f104
Successfully tagged python_app:latest
```

On vérifie que tous les noeuds ont l'image :
```bash
[vagrant@node3 ~]$ sudo docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
python_app          latest              f67e7e81b4bf        5 seconds ago       117MB
alpine              latest              e7d92cdc71fe        5 weeks ago         5.59MB
```

On execute le `docker-compose.yml` :
```bash
[vagrant@node1 ~]$ sudo docker stack deploy -c /data/python_app/docker-compose.yml python-app
Ignoring unsupported options: restart

Creating network python-app_internal
Creating service python-app_redis
Creating service python-app_web
```

On vérifie que les services se sont lancés :
```bash
[vagrant@node1 ~]$ sudo docker stack ls
NAME                SERVICES            ORCHESTRATOR
python-app          2                   Swarm
[vagrant@node1 ~]$ sudo docker service ls
ID                  NAME                MODE                REPLICAS            IMAGE               PORTS
siqo2d9tjdyt        python-app_redis    replicated          1/1                 redis:latest
kinfq1aupagi        python-app_web      replicated          1/1                 python_app:latest   *:8888->8888/tcp
```

🌞 explorer l'application et les fonctionnalités de Swarm

-   tester le bon fonctionnement de l'application
-   utiliser `ss` sur tous les noeuds pour comprendre sur quel port est exposée l'application
On voit que tout les noeuds ont le port 8888 qui est exposés :
```bash
[vagrant@node2 ~]$ ss -6ln
Netid State      Recv-Q Send-Q          Local Address:Port                    Peer Address:Port
udp   UNCONN     0      0                       [::1]:323                             [::]:*
udp   UNCONN     0      0                        [::]:966                             [::]:*
udp   UNCONN     0      0                        [::]:111                             [::]:*
udp   UNCONN     0      0                        [::]:7946                            [::]:*
tcp   LISTEN     0      128                      [::]:8888                            [::]:*
tcp   LISTEN     0      100                     [::1]:25                              [::]:*
tcp   LISTEN     0      128                      [::]:2377                            [::]:*
tcp   LISTEN     0      128                      [::]:7946                            [::]:*
tcp   LISTEN     0      128                      [::]:111                             [::]:*
tcp   LISTEN     0      128                      [::]:22                              [::]:*

[vagrant@node1 ~]$ ss -6ln
Netid State      Recv-Q Send-Q           Local Address:Port                   Peer Address:Port
udp   UNCONN     0      0                        [::1]:323                            [::]:*
udp   UNCONN     0      0                         [::]:944                            [::]:*
udp   UNCONN     0      0                         [::]:111                            [::]:*
udp   UNCONN     0      0                         [::]:7946                           [::]:*
tcp   LISTEN     0      128                       [::]:8888                           [::]:*
tcp   LISTEN     0      100                      [::1]:25                             [::]:*
tcp   LISTEN     0      128                       [::]:2377                           [::]:*
tcp   LISTEN     0      128                       [::]:7946                           [::]:*
tcp   LISTEN     0      128                       [::]:111                            [::]:*
tcp   LISTEN     0      128                       [::]:22                             [::]:*

[vagrant@node3 ~]$ ss -6ln
Netid  State      Recv-Q Send-Q           Local Address:Port                  Peer Address:Port
udp    UNCONN     0      0                        [::1]:323                           [::]:*
udp    UNCONN     0      0                         [::]:976                           [::]:*
udp    UNCONN     0      0                         [::]:111                           [::]:*
udp    UNCONN     0      0                         [::]:7946                          [::]:*
tcp    LISTEN     0      128                       [::]:8888                          [::]:*
tcp    LISTEN     0      100                      [::1]:25                            [::]:*
tcp    LISTEN     0      128                       [::]:2377                          [::]:*
tcp    LISTEN     0      128                       [::]:7946                          [::]:*
tcp    LISTEN     0      128                       [::]:111                           [::]:*
tcp    LISTEN     0      128                       [::]:22                            [::]:*
```
-   utiliser `docker service scale` afin d'augmenter le nombre de services Python
    -   vérifier que c'est ok en visitant l'interface web (l'id du conteneur est print sur la page web)
    -   avec un navigateur ou une commande `curl`
```bash
[vagrant@node1 ~]$ sudo docker service scale python-app_web=3
python-app_web scaled to 3
overall progress: 3 out of 3 tasks
1/3: running   [==================================================>]
2/3: running   [==================================================>]
3/3: running   [==================================================>]
verify: Service converged
[vagrant@node1 ~]$ curl node1.B3:8888
<h1>Add key</h1>
[..]
Host : f7d6e33d77ff

[vagrant@node1 ~]$ curl node1.b3:8888
<h1>Add key</h1>
[..]
Host : 0a4bd5aac8da
```
-   trouver sur quel(s) hôte(s) tournent tous les conteneurs lancés
```bash
[vagrant@node1 ~]$ sudo docker stack ps python-app
ID                  NAME                 IMAGE               NODE                DESIRED STATE       CURRENT STATE           ERROR                         PORTS
wnkq7os7m1td        python-app_web.1     python_app:latest   node2.b3            Running             Running 3 hours ago
evhnvyphwtzo        python-app_redis.1   redis:latest        node1.b3            Running             Running 3 hours ago
j1wyuuotvltg        python-app_web.2     python_app:latest   node3.b3            Running             Running 8 seconds ago
nhdouea2h92c        python-app_web.3     python_app:latest   node1.b3            Running             Running 8 seconds ago
```
-   faire des tests de bascule en coupant Docker/éteignant une machine
Sur `node1`, on coupe docker :
```bash
[vagrant@node1 ~]$ sudo systemctl stop docker
[vagrant@node1 ~]$ sudo systemctl status docker
● docker.service - Docker Application Container Engine
   Loaded: loaded (/usr/lib/systemd/system/docker.service; enabled; vendor preset: disabled)
   Active: inactive (dead) since Mon 2020-02-24 10:45:42 UTC; 9s ago
     Docs: https://docs.docker.com
  Process: 731 ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock (code=exited, status=0/SUCCESS)
 Main PID: 731 (code=exited, status=0/SUCCESS)

Feb 24 10:45:32 node1.b3 dockerd[731]: time="2020-02-24T10:45:32.494515767Z" level=info msg="node1.b3(b9f05652d9e8): Node leave event for b9f05652d9e8/192.168.33.11"
Feb 24 10:45:32 node1.b3 dockerd[731]: time="2020-02-24T10:45:32.547680611Z" level=info msg="Node b9f05652d9e8/192.168.33.11, left gossip cluster"
Feb 24 10:45:32 node1.b3 dockerd[731]: time="2020-02-24T10:45:32.946171505Z" level=warning msg="rmServiceBinding e3ec3d60eb1deb501f32192056f484f2d6981d6e9989b6e1f5e0273b139d09cd possible transient state ok:false entries:0 set:false "
Feb 24 10:45:33 node1.b3 dockerd[731]: time="2020-02-24T10:45:33.010958409Z" level=warning msg="rmServiceBinding 518021b4b6d554ef1560b401ed537bc799e6a57b340218e4c0e90a53da9afda5 possible transient state ok:false entries:0 set:false "
Feb 24 10:45:33 node1.b3 dockerd[731]: time="2020-02-24T10:45:33.081461109Z" level=warning msg="rmServiceBinding c56c280c3d5a1f26d8caa8a8c844bf30a4fff72311ce1ce2be678060e0e4b162 possible transient state ok:false entries:0 set:false "
Feb 24 10:45:42 node1.b3 dockerd[731]: time="2020-02-24T10:45:42.312101569Z" level=info msg="Container a8dbdb7f6777c75bd85c5758807cff52219ff480fa7b4c016b4dbdbd8cdaf455 failed to exit within 10 seconds of signal 15 - using the force"
Feb 24 10:45:42 node1.b3 dockerd[731]: time="2020-02-24T10:45:42.424175279Z" level=info msg="ignoring event" module=libcontainerd namespace=moby topic=/tasks/delete type="*events.TaskDelete"
Feb 24 10:45:42 node1.b3 dockerd[731]: time="2020-02-24T10:45:42.640194373Z" level=warning msg="Error (Unable to complete atomic operation, key modified) deleting object [endpoint wkuk9pjtfrk4388ge6pcimvvv 7d91f27be2d682f302c7255a2b9407c7b93329068...a437f], retrying...."
Feb 24 10:45:42 node1.b3 dockerd[731]: time="2020-02-24T10:45:42.655756630Z" level=info msg="Daemon shutdown complete"
Feb 24 10:45:42 node1.b3 systemd[1]: Stopped Docker Application Container Engine.
Hint: Some lines were ellipsized, use -l to show in full.
```

On peut remarqué que la base redis sur le `node1` est dans l'état `Shutdown`, mais qu'une nouvelle base a était créé sur le `node3`. De même pour le `python_app` qui était sur le `node1`
```bash
[vagrant@node2 ~]$ sudo docker stack ps python-app
ID                  NAME                   IMAGE               NODE                DESIRED STATE       CURRENT STATE                ERROR                         PORTS
zrnqh8em7eg5        python-app_redis.1     redis:latest        node3.b3            Running             Running 43 seconds ago
wnkq7os7m1td        python-app_web.1       python_app:latest   node2.b3            Running             Running about a minute ago
evhnvyphwtzo        python-app_redis.1     redis:latest        node1.b3            Shutdown            Running 3 hours ago
j1wyuuotvltg        python-app_web.2       python_app:latest   node3.b3            Running             Running about a minute ago
iw3nadw5jpw6        python-app_web.3       python_app:latest   node2.b3            Running             Running 47 seconds ago
nhdouea2h92c         \_ python-app_web.3   python_app:latest   node1.b3            Shutdown            Running 3 minutes ago

[vagrant@node2 ~]$ curl node2.b3:8888
<h1>Add key</h1>
[..]
Host : 35ed50a89e3d
```

## III. Construction de l'écosystème

### 1. Registre

🌞 Déployer un registre Docker simple

-   déployez sur `node1` avec une commande `docker-compose up`
    -   utilisez [le `docker-compose.yml` fourni](/it4lik/cloud-b3-2019/-/blob/master/tp/tp3/registry/docker-compose.yml)
    -   il faudra créer les trois répertoires renseignés à l'intérieur
-   configurer les fichier hosts de vos noeuds pour qu'ils puissent joindre le registre sur `registry.b3` (IP de node1 qui pointe vers `registry.b3`)
-   modifier la configuration de votre démon docker pour ajouter l'option `insecure-registries`
    -   car on utilise pas de HTTPS ici

```bash
[vagrant@node1 ~]$ cd /data/registry/
[vagrant@node1 registry]$ docker-compose up -d
WARNING: The Docker Engine you re using is running in swarm mode.

Compose does not use swarm mode to deploy services to multiple nodes in a swarm. All containers will be scheduled on the current node.

To deploy your application across the swarm, use `docker stack deploy`.

Creating network "registry_default" with the default driver
Pulling registry (registry:2)...
2: Pulling from library/registry
486039affc0a: Pull complete
ba51a3b098e6: Pull complete
8bb4c43d6c8e: Pull complete
6f5f453e5f2d: Pull complete
42bc10b72f42: Pull complete
Digest: sha256:7d081088e4bfd632a88e3f3bcd9e007ef44a796fddfe3261407a3f9f04abe1e7
Status: Downloaded newer image for registry:2
Creating registry_registry_1 ... done

[vagrant@node1 ~]$ vi /etc/docker/daemon.json
{
  "insecure-registries" : ["registry.b3:5000"]
}
[vagrant@node1 ~]$ sudo systemctl restart docker
```
> `insecure-registries` à faire sur tous les noeuds

🌞 Tester le registre

-   build l'image contenant l'app Python sur un noeud, en la nommant correctement pour notre registre
-   pousser l'image de l'application Python
-   adapter le `docker-compose.yml` de l'application Python pour utiliser l'image du registre
```bash
[vagrant@node1 ~]$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
python_app          latest              5346dd670f63        15 hours ago        117MB
redis               <none>              44d36d2c2374        3 weeks ago         98.2MB
registry            2                   708bc6af7e5e        4 weeks ago         25.8MB
alpine              latest              e7d92cdc71fe        5 weeks ago         5.59MB
[vagrant@node1 ~]$ docker tag python_app registry.b3:5000/python_app/python_app:test
[vagrant@node1 ~]$ docker images
REPOSITORY                               TAG                 IMAGE ID            CREATED             SIZE
registry.b3:5000/python_app/python_app   test                5346dd670f63        15 hours ago        117MB
python_app                               latest              5346dd670f63        15 hours ago        117MB
redis                                    <none>              44d36d2c2374        3 weeks ago         98.2MB
registry                                 2                   708bc6af7e5e        4 weeks ago         25.8MB
alpine                                   latest              e7d92cdc71fe        5 weeks ago         5.59MB
[vagrant@node1 ~]$ docker push registry.b3:5000/python_app/python_app:test
The push refers to repository [registry.b3:5000/python_app/python_app]
450c0b504fa3: Pushed
7751ee0919a5: Pushed
57e4251a117b: Pushed
3e7d1be9a11f: Pushed
5216338b40a7: Pushed
test: digest: sha256:e925193d61e4f9a7593c58b9983b83b50f81e221d6d2aa0184c0899b10a75f12 size: 1365
```

Vérification que l'on peut pull l'image sur un autre noeud : 
```bash
[vagrant@node2 ~]$ sudo docker pull registry.b3:5000/python_app/python_app:test
test: Pulling from python_app/python_app
c9b1b535fdd9: Already exists
e2053fa53bb5: Pull complete
9137342b73ef: Pull complete
541bf502df55: Pull complete
e0c96d13ca74: Pull complete
Digest: sha256:e925193d61e4f9a7593c58b9983b83b50f81e221d6d2aa0184c0899b10a75f12
Status: Downloaded newer image for registry.b3:5000/python_app/python_app:test
registry.b3:5000/python_app/python_app:test
```

Le `docker-compose.yml` adapté :
```bash
[vagrant@node1 ~]$ vi /data/python_app/docker-compose.yml
version: '3.7'

services:
  web:
    image: registry.b3:5000/python_app/python_app:test
    deploy:
      restart_policy:
        condition: on-failure
        delay: 5s
        max_attempts: 3
        window: 120s
    networks:
      internal:
        aliases:
          - python-app
    depends_on:
      - redis
    ports:
      - "8888:8888"

  redis:
    image: redis
    networks:
      internal:
        aliases:
          - db

networks:
  internal:

[vagrant@node1 ~]$ sudo docker stack deploy -c /data/python_app/docker-compose.yml python-app
Creating network python-app_internal
Creating service python-app_web
Creating service python-app_redis

[vagrant@node1 ~]$ sudo docker stack ls
NAME                SERVICES            ORCHESTRATOR
python-app          2                   Swarm

[vagrant@node1 ~]$ sudo docker stack ps python-app
ID                  NAME                 IMAGE                                         NODE                DESIRED STATE       CURRENT STATE            ERROR               PORTS
jywlhot76ch4        python-app_redis.1   redis:latest                                  node1.b3            Running             Running 16 seconds ago
ipnwu5v0vcwv        python-app_web.1     registry.b3:5000/python_app/python_app:test   node3.b3            Running             Running 11 seconds ago
```

### 2. Centraliser l'accès aux services
🌞 **Déployer une _stack_ Traefik**

-   créer un réseau dédié à Traefik
    -   il sera utilisé pour Traefik
    -   toutes les applications qui auront besoin du reverse proxy Traefik seront aussi dans ce réseau
```bash
[vagrant@node1 ~]$ docker network create --driver overlay traefik
uctgfcovjx1wuycurm18grydl
[vagrant@node1 ~]$ docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
7101f9da76dc        bridge              bridge              local
9f5e24826b5e        docker_gwbridge     bridge              local
b5225c089565        host                host                local
wkuk9pjtfrk4        ingress             overlay             swarm
c590ff8450d8        none                null                local
e7f2a22a5525        registry_default    bridge              local
uctgfcovjx1w        traefik             overlay             swarm
```

🌞 **Déployer une _stack_ Traefik**

-   utiliser [le `docker-compose.yml` fourni](/it4lik/cloud-b3-2019/-/blob/master/tp/tp3/traefik/docker-compose.yml)
    -   modifier le `docker-compose.yml` pour partager le port 8080
    -   noter l'utilisation du réseau `traefik` en `external`
        -   cela indique qu'il doit être créé auparavant
        -   ce réseau sera aussi utilisé pour les prochaines applications que l'on mettra derrière le reverse proxy
-   vérifier le bon fonctionnement en visitant l'interface web de Traefik
    -   grâce au partage de port
    -   `http://<IP_VM>:8080/dashboard/`

Le `docker-compose.yml` :
```bash
version: '3'

services:
  reverse-proxy:
    image: traefik:v2.1.3
    command:
      - "--providers.docker.endpoint=unix:///var/run/docker.sock"
      - "--providers.docker.swarmMode=true"
      - "--providers.docker.exposedbydefault=false"
      - "--providers.docker.network=traefik"
      - "--entrypoints.web.address=:80"
      - "--api.dashboard=true"
      - "--api.insecure=true"
    ports:
      - 8080:8080
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
    networks:
      - traefik
    deploy:
      placement:
        constraints:
          - node.role == manager

networks:
  traefik:
    external: true
```

Déploiement de `traefik` et vérification avec un curl :
```bash
[vagrant@node1 ~]$ sudo docker stack deploy -c /data/traefik/docker-compose.yml traefik
Creating service traefik_reverse-proxy
[vagrant@node1 ~]$ docker stack ls
NAME                SERVICES            ORCHESTRATOR
traefik             1                   Swarm
[vagrant@node1 ~]$ docker stack ps traefik
ID                  NAME                      IMAGE               NODE                DESIRED STATE       CURRENT STATE            ERROR               PORTS
6kgmiy8x1b0y        traefik_reverse-proxy.1   traefik:v2.1.3      node3.b3            Running             Running 34 seconds ago
[vagrant@node1 ~]$ curl node1.b3:8080/dashboard
<!DOCTYPE html><html><head><title>Traefik</title><meta charset=utf-8><meta name=description content="Traefik UI"><meta name=format-detection content="telephone=no"><meta name=msapplication-tap-highlight content=no><meta name=viewport content="user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1,width=device-width"><link rel=icon type=image/png href=statics/app-logo-128x128.png><link rel=icon type=image/png sizes=16x16 href=statics/icons/favicon-16x16.png><link rel=icon type=image/png sizes=32x32 href=statics/icons/favicon-32x32.png><link rel=icon type=image/png sizes=96x96 href=statics/icons/favicon-96x96.png><link rel=icon type=image/ico href=statics/icons/favicon.ico><link href=css/019be8e4.d05f1162.css rel=prefetch><link href=css/099399dd.9310dd1b.css rel=prefetch><link href=css/0af0fca4.e3d6530d.css rel=prefetch><link href=css/162d302c.9310dd1b.css rel=prefetch><link href=css/29ead7f5.9310dd1b.css rel=prefetch><link href=css/31ad66a3.9310dd1b.css rel=prefetch><link href=css/524389aa.619bfb84.css rel=prefetch><link href=css/61674343.9310dd1b.css rel=prefetch><link href=css/63c47f2b.294d1efb.css rel=prefetch><link href=css/691c1182.ed0ee510.css rel=prefetch><link href=css/7ba452e3.37efe53c.css rel=prefetch><link href=css/87fca1b4.8c8c2eec.css rel=prefetch><link href=js/019be8e4.d8726e8b.js rel=prefetch><link href=js/099399dd.a047d401.js rel=prefetch><link href=js/0af0fca4.271bd48d.js rel=prefetch><link href=js/162d302c.ce1f9159.js rel=prefetch><link href=js/29ead7f5.cd022784.js rel=prefetch><link href=js/2d21e8fd.f3d2bb6c.js rel=prefetch><link href=js/31ad66a3.12ab3f06.js rel=prefetch><link href=js/524389aa.21dfc9ee.js rel=prefetch><link href=js/61674343.adb358dd.js rel=prefetch><link href=js/63c47f2b.caf9b4a2.js rel=prefetch><link href=js/691c1182.5d4aa4c9.js rel=prefetch><link href=js/7ba452e3.71a69a60.js rel=prefetch><link href=js/87fca1b4.ac9c2dc6.js rel=prefetch><link href=css/app.e4fba3f1.css rel=preload as=style><link href=js/app.841031a8.js rel=preload as=script><link href=js/vendor.49a1849c.js rel=preload as=script><link href=css/app.e4fba3f1.css rel=stylesheet><link rel=manifest href=manifest.json><meta name=theme-color content=#027be3><meta name=apple-mobile-web-app-capable content=yes><meta name=apple-mobile-web-app-status-bar-style content=default><meta name=apple-mobile-web-app-title content=Traefik><link rel=apple-touch-icon href=statics/icons/apple-icon-120x120.png><link rel=apple-touch-icon sizes=180x180 href=statics/icons/apple-icon-180x180.png><link rel=apple-touch-icon sizes=152x152 href=statics/icons/apple-icon-152x152.png><link rel=apple-touch-icon sizes=167x167 href=statics/icons/apple-icon-167x167.png><link rel=mask-icon href=statics/icons/safari-pinned-tab.svg color=#027be3><meta name=msapplication-TileImage content=statics/icons/ms-icon-144x144.png><meta name=msapplication-TileColor content=#000000></head><body><div id=q-app></div><script type=text/javascript src=js/app.841031a8.js></script><script type=text/javascript src=js/vendor.49a1849c.js></script></body></html>
[vagrant@node1 ~]$
```

🌞 **Passer l'application Web Python derrière Traefik**

-   réutiliser encore le `docker-compose.yml` de l'app Python
    -   positionner des labels dans le `docker-compose.yml` afin que Traefik détecte l'application
    -   **enlever le partage de ports** que vous aviez mis
    -   préciser l'utilisation du réseau `traefik` en `external` (référez-vous au [`docker-compose.yml` de traefik](/it4lik/cloud-b3-2019/-/blob/master/tp/tp3/traefik/docker-compose.yml) pour la syntaxe)
-   l'application sera accessible _via_ un nom de domaine
    -   le `<DOMAIN_NAME>` choisi dans l'exemple ci-dessous
    -   sans serveur DNS, **il faudra ajouter le nom à votre fichier hosts**
-   tester
    -   avec un `curl` ou un navigateur web

Le `docker-compose.yml` :
```yml
version: '3.7'

services:
  python-webapp:
    image: registry.b3:5000/python_app/python_app:test
    deploy:
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.python-webapp.rule=Host(`python-webapp`)"
        - "traefik.http.routers.python-webapp.entrypoints=web"
        - "traefik.http.services.python-webapp.loadbalancer.server.port=8888"
      restart_policy:
        condition: on-failure
        delay: 5s
        max_attempts: 3
        window: 120s
    networks:
       - traefik
    depends_on:
      - redis
  redis:
    image: redis
    networks:
      - traefik
networks:
  traefik:
    external: true
```

Vérification sur l'hôte :
```bash
C:\Users\Kyoshin>curl http://python-webapp
<h1>Add key</h1>
<form action="/add" method = "POST">

Key:
<input type="text" name="key" >

Value:
<input type="text" name="value" >

<input type="submit" value="Submit">
</form>

<h1>Check key</h1>
<form action="/get" method = "POST">

Key:
<input type="text" name="key" >
<input type="submit" value="Submit">
</form>

Host : 27a0f0431ada
```

🌞 **Mettre le dashboard de Traefik derrière le proxy**

-   pour ce faire, modifier le `docker-compose.yml` de Traefik
    -   ajouter des labels sur le conteneur Traefik
-   relancer la stack Traefik

Le nouveau ficher `docker-compose.yml` de Traefik :
```yml
version: '3'

services:
  reverse-proxy:
    image: traefik:v2.1.3
    command:
      - "--providers.docker.endpoint=unix:///var/run/docker.sock"
      - "--providers.docker.swarmMode=true"
      - "--providers.docker.exposedbydefault=false"
      - "--providers.docker.network=traefik"
      - "--entrypoints.web.address=:80"
      - "--api.dashboard=true"
      - "--api.insecure=true"
    ports:
      - 80:80
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
    networks:
      - traefik
    deploy:
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.traefik.rule=Host(`traefik`)"
        - "traefik.http.routers.traefik.entrypoints=web"
        - "traefik.http.services.traefik.loadbalancer.server.port=8080"
      placement:
        constraints:
          - node.role == manager

networks:
  traefik:
    external: true
```

Déploiement :
```bash
[vagrant@node1 ~]$ docker stack rm traefik
Removing service traefik_reverse-proxy
[vagrant@node1 ~]$ sudo docker stack deploy -c /data/traefik/docker-compose.yml traefik
Creating service traefik_reverse-proxy
```

Vérification :
```bash
C:\Users\Kyoshin>curl http://traefik/dashboard/#/
<!DOCTYPE html><html><head><title>Traefik</title><meta charset=utf-8><meta name=description content="Traefik UI"><meta name=format-detection content="telephone=no"><meta name=msapplication-tap-highlight content=no><meta name=viewport content="user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1,width=device-width"><link rel=icon type=image/png href=statics/app-logo-128x128.png><link rel=icon type=image/png sizes=16x16 href=statics/icons/favicon-16x16.png><link rel=icon type=image/png sizes=32x32 href=statics/icons/favicon-32x32.png><link rel=icon type=image/png sizes=96x96 href=statics/icons/favicon-96x96.png><link rel=icon type=image/ico href=statics/icons/favicon.ico><link href=css/019be8e4.d05f1162.css rel=prefetch><link href=css/099399dd.9310dd1b.css rel=prefetch><link href=css/0af0fca4.e3d6530d.css rel=prefetch><link href=css/162d302c.9310dd1b.css rel=prefetch><link href=css/29ead7f5.9310dd1b.css rel=prefetch><link href=css/31ad66a3.9310dd1b.css rel=prefetch><link href=css/524389aa.619bfb84.css rel=prefetch><link href=css/61674343.9310dd1b.css rel=prefetch><link href=css/63c47f2b.294d1efb.css rel=prefetch><link href=css/691c1182.ed0ee510.css rel=prefetch><link href=css/7ba452e3.37efe53c.css rel=prefetch><link href=css/87fca1b4.8c8c2eec.css rel=prefetch><link href=js/019be8e4.d8726e8b.js rel=prefetch><link href=js/099399dd.a047d401.js rel=prefetch><link href=js/0af0fca4.271bd48d.js rel=prefetch><link href=js/162d302c.ce1f9159.js rel=prefetch><link href=js/29ead7f5.cd022784.js rel=prefetch><link href=js/2d21e8fd.f3d2bb6c.js rel=prefetch><link href=js/31ad66a3.12ab3f06.js rel=prefetch><link href=js/524389aa.21dfc9ee.js rel=prefetch><link href=js/61674343.adb358dd.js rel=prefetch><link href=js/63c47f2b.caf9b4a2.js rel=prefetch><link href=js/691c1182.5d4aa4c9.js rel=prefetch><link href=js/7ba452e3.71a69a60.js rel=prefetch><link href=js/87fca1b4.ac9c2dc6.js rel=prefetch><link href=css/app.e4fba3f1.css rel=preload as=style><link href=js/app.841031a8.js rel=preload as=script><link href=js/vendor.49a1849c.js rel=preload as=script><link href=css/app.e4fba3f1.css rel=stylesheet><link rel=manifest href=manifest.json><meta name=theme-color content=#027be3><meta name=apple-mobile-web-app-capable content=yes><meta name=apple-mobile-web-app-status-bar-style content=default><meta name=apple-mobile-web-app-title content=Traefik><link rel=apple-touch-icon href=statics/icons/apple-icon-120x120.png><link rel=apple-touch-icon sizes=180x180 href=statics/icons/apple-icon-180x180.png><link rel=apple-touch-icon sizes=152x152 href=statics/icons/apple-icon-152x152.png><link rel=apple-touch-icon sizes=167x167 href=statics/icons/apple-icon-167x167.png><link rel=mask-icon href=statics/icons/safari-pinned-tab.svg color=#027be3><meta name=msapplication-TileImage content=statics/icons/ms-icon-144x144.png><meta name=msapplication-TileColor content=#000000></head><body><div id=q-app></div><script type=text/javascript src=js/app.841031a8.js></script><script type=text/javascript src=js/vendor.49a1849c.js></script></body></html>
C:\Users\Kyoshin>
```

### 3. Stockage S3

🌞 **Préparer l'environnement**

-   utiliser LVM pour partitionner les disques supplémentaires et les monter dans leurs paths respectifs

Monter les partitions `/minio` :
```bash
[vagrant@node1 ~]$ sudo fdisk -l

Disk /dev/sda: 42.9 GB, 42949672960 bytes, 83886080 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: dos
Disk identifier: 0x0009ef88

   Device Boot      Start         End      Blocks   Id  System
/dev/sda1   *        2048    83886079    41942016   83  Linux

Disk /dev/sdb: 5368 MB, 5368709120 bytes, 10485760 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes

[vagrant@node1 ~]$ sudo pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.
[vagrant@node1 ~]$ sudo pvdisplay
  "/dev/sdb" is a new physical volume of "5.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb
  VG Name
  PV Size               5.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               ZVZPN3-JsHf-piKr-6udh-YFfe-aEmO-Z2YQJL

[vagrant@node1 ~]$ sudo vgcreate VGMinio /dev/sdb
  Volume group "VGMinio" successfully created
[vagrant@node1 ~]$ sudo vgdisplay
  --- Volume group ---
  VG Name               VGMinio
  System ID
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <5.00 GiB
  PE Size               4.00 MiB
  Total PE              1279
  Alloc PE / Size       0 / 0
  Free  PE / Size       1279 / <5.00 GiB
  VG UUID               MrxG5D-craE-o22N-UhkJ-b2XT-vec3-XMdLvP

[vagrant@node1 ~]$ sudo lvcreate -n Minio -L 4.9go VGMinio
  Rounding up size to full physical extent 4.90 GiB
  Logical volume "Minio" created.

[vagrant@node1 ~]$ sudo mkfs -t ext4 /dev/mapper/VGMinio-Minio
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
321280 inodes, 1285120 blocks
64256 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=1317011456
40 block groups
32768 blocks per group, 32768 fragments per group
8032 inodes per group
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736

Allocating group tables: done
Writing inode tables: done
Creating journal (32768 blocks): done
Writing superblocks and filesystem accounting information: done

[vagrant@node1 ~]$ sudo mkdir /minio
[vagrant@node1 ~]$ sudo mount /dev/mapper/VGMinio-Minio /minio
[vagrant@node1 ~]$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0   40G  0 disk
└─sda1            8:1    0   40G  0 part /
sdb               8:16   0    5G  0 disk
└─VGMinio-Minio 253:0    0  4.9G  0 lvm  /minio
```
Résultat :
```bash
[vagrant@node1 ~]$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0   40G  0 disk
└─sda1            8:1    0   40G  0 part /
sdb               8:16   0    5G  0 disk
└─VGMinio-Minio 253:0    0  4.9G  0 lvm  /minio

[vagrant@node2 ~]$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0   40G  0 disk
└─sda1            8:1    0   40G  0 part /
sdb               8:16   0    5G  0 disk
└─VGMinio-Minio 253:0    0  4.9G  0 lvm  /minio

[vagrant@node3 ~]$ lsblk
NAME                  MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda                     8:0    0   40G  0 disk
└─sda1                  8:1    0   40G  0 part /
sdb                     8:16   0    5G  0 disk
└─VGMinio-Minio       253:0    0  4.9G  0 lvm  /minio
sdc                     8:32   0    5G  0 disk
└─VGMinio--2-Minio--2 253:1    0  4.9G  0 lvm  /minio-2
```

🌞 **Déployer Minio**

Ajout des des labels sur chaque noeud :
```powershell
[vagrant@node1 ~]$ docker node update --label-add minio1=true node1.b3
node1.b3
[vagrant@node1 ~]$ docker node update --label-add minio2=true node2.b3
node2.b3
[vagrant@node1 ~]$ docker node update --label-add minio3=true node3.b3
node3.b3
[vagrant@node1 ~]$ docker node update --label-add minio4=true node3.b3
node3.b3
```

Ajout des Docker secret pour MinIO :
```
echo "AKIAIOSFODNN7EXAMPLE" | docker secret create access_key -
echo "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY" | docker secret create secret_key -
```


Le `docker-compose.yml` pour déployer minio :
```yml
version: '3.7'

services:
  minio1:
    image: minio/minio:RELEASE.2020-03-14T02-21-58Z
    hostname: minio1
    volumes:
      - minio1-data:/export
    networks:
      - minio_distributed
      - traefik
    deploy:
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.traefik.rule=Host(`minio1`)"
        - "traefik.http.routers.traefik.entrypoints=web"
        - "traefik.http.services.traefik.loadbalancer.server.port=9000"
      restart_policy:
        delay: 10s
        max_attempts: 10
        window: 60s
      placement:
        constraints:
          - node.labels.minio1==true
    command: server http://minio{1...4}/export
    secrets:
      - secret_key
      - access_key
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:9000/minio/health/live"]
      interval: 30s
      timeout: 20s
      retries: 3

  minio2:
    image: minio/minio:RELEASE.2020-03-14T02-21-58Z
    hostname: minio2
    volumes:
      - minio2-data:/export
    networks:
      - minio_distributed
      - traefik
    deploy:
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.traefik.rule=Host(`minio2`)"
        - "traefik.http.routers.traefik.entrypoints=web"
        - "traefik.http.services.traefik.loadbalancer.server.port=9000"
      restart_policy:
        delay: 10s
        max_attempts: 10
        window: 60s
      placement:
        constraints:
          - node.labels.minio2==true
    command: server http://minio{1...4}/export
    secrets:
      - secret_key
      - access_key
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:9000/minio/health/live"]
      interval: 30s
      timeout: 20s
      retries: 3

  minio3:
    image: minio/minio:RELEASE.2020-03-14T02-21-58Z
    hostname: minio3
    volumes:
      - minio3-data:/export
    networks:
      - minio_distributed
      - traefik
    deploy:
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.traefik.rule=Host(`minio3`)"
        - "traefik.http.routers.traefik.entrypoints=web"
        - "traefik.http.services.traefik.loadbalancer.server.port=9000"
      restart_policy:
        delay: 10s
        max_attempts: 10
        window: 60s
      placement:
        constraints:
          - node.labels.minio3==true
    command: server http://minio{1...4}/export
    secrets:
      - secret_key
      - access_key
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:9000/minio/health/live"]
      interval: 30s
      timeout: 20s
      retries: 3

  minio4:
    image: minio/minio:RELEASE.2020-03-14T02-21-58Z
    hostname: minio4
    volumes:
      - minio4-data:/export
    networks:
      - minio_distributed
      - traefik
    deploy:
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.traefik.rule=Host(`minio4`)"
        - "traefik.http.routers.traefik.entrypoints=web"
        - "traefik.http.services.traefik.loadbalancer.server.port=9000"
      restart_policy:
        delay: 10s
        max_attempts: 10
        window: 60s
      placement:
        constraints:
          - node.labels.minio4==true
    command: server http://minio{1...4}/export
    secrets:
      - secret_key
      - access_key
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:9000/minio/health/live"]
      interval: 30s
      timeout: 20s
      retries: 3

volumes:
  minio1-data:

  minio2-data:

  minio3-data:

  minio4-data:

networks:
  minio_distributed:
    driver: overlay
  traefik:
    external: true

secrets:
  secret_key:
    external: true
  access_key:
    external: true
```