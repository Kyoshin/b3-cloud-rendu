# TP 2 - Containerization in-depth
Auteur : Jonathan DINH<br/>
Classe : B3B<br/>
Date de création : 07/01/2020<br/>

## I. Gestion de conteneurs Docker
- 🌞 Mettre en évidence l'utilisation de chacun des processus liés à Docker
  -   `dockerd`, `containerd`, `containerd-shim`
  -   analyser qui est le père de qui (en terme de processus, avec leurs PIDs)
  -   avec la commande `ps` par exemple
```bash
[root@localhost ~]# ps -edf | grep containerd
root      1110     1  0 11:29 ?        00:00:00 /usr/bin/containerd
root      1111     1  0 11:29 ?        00:00:01 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
root      1438  1110  0 11:31 ?        00:00:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/f51259a7af540861c07d3723dcee456919c656fb10ec04dce7d8e5454c42519d -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-runc
```

On voit `dockerd` en deuxième ligne avec le **PID** `1111`. Alors que le `containerd` est en première ligne avec le **PID** `1110`. Les deux ont leurs processus père le kernel (**PID** `1`). Enfin la troisième ligne, c'est le `containerd-shim` avec le **PID** `1438` et son père est `containerd` (**PID** `1110`).

On remarque aussi que `containerd` est lié à `dockerd` car `dockerd` utilise le socket de `containerd` :
```bash
root      1111     1  0 11:29 ?        00:00:01 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
```

- 🌞 Utiliser l'API HTTP mise à disposition par `dockerd`
  -   utiliser un `curl` (ou autre) pour discuter à travers le socker UNIX
  -   la [documentation de l'API est dispo en ligne](https://docs.docker.com/engine/api/v1.40/)
  -   récupérer la liste des conteneurs
   ```
  [root@localhost ~]# curl --unix-socket /var/run/docker.sock http:/containers/json
  [{"Id":"36e40413d2c1b0f2a1e19262ee371f91ed101bf71dea78d1f096a6bcf34bbca6","Names":["/xenodochial_sutherland"],"Image":"alpine","ImageID":"sha256:cc0abc535e36a7ede71978ba2bbd8159b8a5420b91f2fbc520cdf5f673640a34","Command":"sleep 9999","Created":1578396671,"Ports":[],"Labels":{},"State":"running","Status":"Up 8 minutes","HostConfig":{"NetworkMode":"default"},"NetworkSettings":{"Networks":{"bridge":{"IPAMConfig":null,"Links":null,"Aliases":null,"NetworkID":"f76f0ed7a51817135cfa639f9ebdb3d593248dc463094883516433c446728833","EndpointID":"f05c98e28f1fd33e9db0ce30eee65658e356d72b2452c4cb9d851a09c125719d","Gateway":"172.17.0.1","IPAddress":"172.17.0.2","IPPrefixLen":16,"IPv6Gateway":"","GlobalIPv6Address":"","GlobalIPv6PrefixLen":0,"MacAddress":"02:42:ac:11:00:02","DriverOpts":null}}},"Mounts":[]}]
  ```
  -   récupérer la liste des images disponibles
  ```
  [root@localhost ~]# curl --unix-socket /var/run/docker.sock http:/images/json
  [{"Containers":-1,"Created":1577215212,"Id":"sha256:cc0abc535e36a7ede71978ba2bbd8159b8a5420b91f2fbc520cdf5f673640a34","Labels":null,"ParentId":"","RepoDigests":["alpine@sha256:2171658620155679240babee0a7714f6509fae66898db422ad803b951257db78"],"RepoTags":["alpine:latest"],"SharedSize":-1,"Size":5591300,"VirtualSize":5591300}]
  ```

## II. Sandboxing
### 1. Namespaces
#### A. Exploration manuelle
🌞 Trouver les namespaces utilisés par votre shell.
```
[root@localhost ~]# ls -al /proc/$$/ns
total 0
dr-x--x--x. 2 root root 0 Jan  7 12:54 .
dr-xr-xr-x. 9 root root 0 Jan  7 11:29 ..
lrwxrwxrwx. 1 root root 0 Jan  7 12:54 ipc -> ipc:[4026531839]
lrwxrwxrwx. 1 root root 0 Jan  7 12:54 mnt -> mnt:[4026531840]
lrwxrwxrwx. 1 root root 0 Jan  7 12:54 net -> net:[4026531956]
lrwxrwxrwx. 1 root root 0 Jan  7 12:54 pid -> pid:[4026531836]
lrwxrwxrwx. 1 root root 0 Jan  7 12:54 user -> user:[4026531837]
lrwxrwxrwx. 1 root root 0 Jan  7 12:54 uts -> uts:[4026531838]
```

#### B. `unshare`

🌞 Créer un pseudo-conteneur à la main en utilisant `unshare`
-   lancer une commande `unshare`
```bash
[root@localhost ~]# ps -edf | grep bash
root      1368  1359  0 11:29 pts/0    00:00:00 -bash
root      3118  3110  0 12:28 pts/1    00:00:00 -bash
[root@localhost ~]# unshare
```
-   `unshare` doit exécuter le processus `bash`
```bash
[root@localhost ~]# ps -edf | grep bash
root      1368  1359  0 11:29 pts/0    00:00:00 -bash
root      3118  3110  0 12:28 pts/1    00:00:00 -bash
root      3688  3652  0 13:01 pts/0    00:00:00 -bash
```
-   ce processus doit utiliser des namespaces différents de votre hôte :
    -   réseau
    -   mount
    -   PID
    -   user
```bash
[root@localhost ~]# unshare -fmnpUr
```
Les options `f` et `r` pour évité ces erreurs :
```bash
/usr/bin/id: cannot find name for user ID 65534
-bash: fork: Cannot allocate memory
```

-   prouver depuis votre `bash` isolé que ces namespaces sont bien mis en place
    - dans un autre `bash` : 
    ```bash
    [root@localhost ~]# ps -edf |grep bash
    root      1433  1424  0 13:44 pts/0    00:00:00 -bash
    root      1524  1523  0 13:47 pts/0    00:00:00 -bash
    root      1567  1558  0 13:49 pts/1    00:00:00 -bash
    [root@localhost ~]# ls -al /proc/$$/ns
    total 0
    dr-x--x--x. 2 root root 0 Jan  7 13:54 .
    dr-xr-xr-x. 9 root root 0 Jan  7 13:49 ..
    lrwxrwxrwx. 1 root root 0 Jan  7 13:54 ipc -> ipc:[4026531839]
    lrwxrwxrwx. 1 root root 0 Jan  7 13:54 mnt -> mnt:[4026531840]
    lrwxrwxrwx. 1 root root 0 Jan  7 13:54 net -> net:[4026531956]
    lrwxrwxrwx. 1 root root 0 Jan  7 13:54 pid -> pid:[4026531836]
    lrwxrwxrwx. 1 root root 0 Jan  7 13:54 user -> user:[4026531837]
    lrwxrwxrwx. 1 root root 0 Jan  7 13:54 uts -> uts:[4026531838]
    ```
    - dans le `bash` **isolé** :
    ```
    [root@localhost ~]# ls -al /proc/1524/ns
    total 0
    dr-x--x--x. 2 root root 0 Jan  7 13:55 .
    dr-xr-xr-x. 9 root root 0 Jan  7 13:47 ..
    lrwxrwxrwx. 1 root root 0 Jan  7 13:55 ipc -> ipc:[4026531839]
    lrwxrwxrwx. 1 root root 0 Jan  7 13:55 mnt -> mnt:[4026532189]
    lrwxrwxrwx. 1 root root 0 Jan  7 13:55 net -> net:[4026532192]
    lrwxrwxrwx. 1 root root 0 Jan  7 13:55 pid -> pid:[4026532190]
    lrwxrwxrwx. 1 root root 0 Jan  7 13:55 user -> user:[4026532188]
    lrwxrwxrwx. 1 root root 0 Jan  7 13:55 uts -> uts:[4026531838]
    ``` 

#### C. Avec docker

```bash
[root@localhost ~]# docker run -d alpine sleep 9999
eb94cd2d3845adad5ef6ccaefc4028337da93ddd75d3555db898b0aabdcf5b10
```

🌞 Trouver dans quels namespaces ce conteneur s'exécute.

```bash
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
5d42443eb800        debian              "sleep 99999"       6 minutes ago       Up 6 minutes                            vibrant_buck
eb94cd2d3845        alpine              "sleep 9999"        14 minutes ago      Up 14 minutes                           sleepy_northcutt
[root@localhost ~]# docker inspect -f '{{.State.Pid}}' 5d42443eb800
1798
[root@localhost ~]# ls -al /proc/1798/ns
total 0
dr-x--x--x. 2 root root 0 Jan  7 14:14 .
dr-xr-xr-x. 9 root root 0 Jan  7 14:14 ..
lrwxrwxrwx. 1 root root 0 Jan  7 14:20 ipc -> ipc:[4026532192]
lrwxrwxrwx. 1 root root 0 Jan  7 14:20 mnt -> mnt:[4026532190]
lrwxrwxrwx. 1 root root 0 Jan  7 14:14 net -> net:[4026532195]
lrwxrwxrwx. 1 root root 0 Jan  7 14:20 pid -> pid:[4026532193]
lrwxrwxrwx. 1 root root 0 Jan  7 14:20 user -> user:[4026531837]
lrwxrwxrwx. 1 root root 0 Jan  7 14:20 uts -> uts:[4026532191]
```

#### D. `nsenter`

🌞 Utiliser `nsenter` pour rentrer dans les namespaces de votre conteneur en y exécutant un shell
-   prouver que vous êtes isolé en terme de réseau, arborescence de processus, points de montage
```bash
[root@localhost ~]# nsenter --target 1798 --mount --uts --ipc --net --pid
mesg: ttyname failed: No such device
root@5d42443eb800:/# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
7: eth0@if8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
    link/ether 02:42:ac:11:00:03 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.3/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever

root@5d42443eb800:/# df -h
Filesystem               Size  Used Avail Use% Mounted on
overlay                  6.2G  2.5G  3.7G  41% /
tmpfs                     64M     0   64M   0% /dev
tmpfs                    496M     0  496M   0% /sys/fs/cgroup
shm                       64M     0   64M   0% /dev/shm
/dev/mapper/centos-root  6.2G  2.5G  3.7G  41% /etc/hosts
tmpfs                    496M     0  496M   0% /proc/asound
tmpfs                    496M     0  496M   0% /proc/acpi
tmpfs                    496M     0  496M   0% /proc/scsi
tmpfs                    496M     0  496M   0% /sys/firmware

root@5d42443eb800:/# exit

[root@localhost ~]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:be:0d:e8 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
       valid_lft 83039sec preferred_lft 83039sec
    inet6 fe80::2b83:fc54:3571:2f6f/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:43:6f:a0 brd ff:ff:ff:ff:ff:ff
    inet 192.168.238.3/24 brd 192.168.238.255 scope global noprefixroute dynamic enp0s8
       valid_lft 1013sec preferred_lft 1013sec
    inet6 fe80::3516:5ce1:4d6f:f0e5/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
4: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
    link/ether 02:42:c7:fc:f0:8c brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:c7ff:fefc:f08c/64 scope link
       valid_lft forever preferred_lft forever
6: veth46cf148@if5: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default
    link/ether 8a:57:49:4b:bc:b0 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet6 fe80::8857:49ff:fe4b:bcb0/64 scope link
       valid_lft forever preferred_lft forever
8: veth56a02da@if7: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default
    link/ether f6:78:eb:4d:87:b4 brd ff:ff:ff:ff:ff:ff link-netnsid 1
    inet6 fe80::f478:ebff:fe4d:87b4/64 scope link
       valid_lft forever preferred_lft forever

[root@localhost ~]# df -h
Filesystem               Size  Used Avail Use% Mounted on
devtmpfs                 484M     0  484M   0% /dev
tmpfs                    496M     0  496M   0% /dev/shm
tmpfs                    496M  7.0M  489M   2% /run
tmpfs                    496M     0  496M   0% /sys/fs/cgroup
/dev/mapper/centos-root  6.2G  2.5G  3.7G  41% /
/dev/sda1               1014M  186M  829M  19% /boot
tmpfs                    100M     0  100M   0% /run/user/0
overlay                  6.2G  2.5G  3.7G  41% /data/docker/overlay2/867155c29c45f60f237aa2223d4353f8e0c7c68cb24d107d5d88e953875bf7dd/merged
overlay                  6.2G  2.5G  3.7G  41% /data/docker/overlay2/476d2384ca22aac37675e043fb3cc8f28a0c80f0beb7d2c8b748b9f5cdc8dc26/merged
```

#### E. Et alors, les namespaces User ?

🌞 Mettez en place la configuration nécessaire pour que Docker utilise les namespaces de type User.

```bash
[root@localhost ~]# vim /etc/subuid
testuser:231072:65536
[root@localhost ~]# vim /etc/subgid
testuser:231072:65536
[root@localhost ~]# adduser testuser
[root@localhost ~]# id testuser
uid=1001(testuser) gid=1001(testuser) groups=1001(testuser)
[root@localhost ~]# vim /etc/docker/daemon.json
{
  "userns-remap": "testuser"
}
[root@localhost ~]# systemctl restart docker
[root@localhost ~]# docker run -d alpine sleep 9999
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
f0b2fbd75fc8        alpine              "sleep 9999"        43 seconds ago      Up 42 seconds                           eager_wiles
[root@localhost ~]# docker inspect -f '{{.State.Pid}}' f0b2fbd75fc8
2673
[root@localhost ~]# ls -al /proc/2673/ns
total 0
dr-x--x--x. 2 231072 231072 0 Jan  7 16:26 .
dr-xr-xr-x. 9 231072 231072 0 Jan  7 16:26 ..
lrwxrwxrwx. 1 231072 231072 0 Jan  7 16:27 ipc -> ipc:[4026532193]
lrwxrwxrwx. 1 231072 231072 0 Jan  7 16:27 mnt -> mnt:[4026532191]
lrwxrwxrwx. 1 231072 231072 0 Jan  7 16:26 net -> net:[4026532196]
lrwxrwxrwx. 1 231072 231072 0 Jan  7 16:27 pid -> pid:[4026532194]
lrwxrwxrwx. 1 231072 231072 0 Jan  7 16:27 user -> user:[4026532190]
lrwxrwxrwx. 1 231072 231072 0 Jan  7 16:27 uts -> uts:[4026532192]
[root@localhost ~]# ls -al /proc/$$/ns
total 0
dr-x--x--x. 2 root root 0 Jan  7 13:54 .
dr-xr-xr-x. 9 root root 0 Jan  7 13:49 ..
lrwxrwxrwx. 1 root root 0 Jan  7 13:54 ipc -> ipc:[4026531839]
lrwxrwxrwx. 1 root root 0 Jan  7 13:54 mnt -> mnt:[4026531840]
lrwxrwxrwx. 1 root root 0 Jan  7 13:54 net -> net:[4026531956]
lrwxrwxrwx. 1 root root 0 Jan  7 13:54 pid -> pid:[4026531836]
lrwxrwxrwx. 1 root root 0 Jan  7 13:54 user -> user:[4026531837]
lrwxrwxrwx. 1 root root 0 Jan  7 13:54 uts -> uts:[4026531838]
```

On remarque que le namespace de type User est différent.

```
[root@localhost ~]# ps -edf | grep 2673
231072    2673  2657  0 16:26 ?        00:00:00 sleep 9999
```

On remarque aussi que le user du **PID** `2673` est `231072`

#### F. Isolation réseau
- 🌞 lancer un conteneur simple
    - ajouter une option pour partager un port (n'importe lequel), pour voir plus d'informations
    ```bash
    [jdinh@localhost ~]$ sudo docker run -d -p 8888:7777 debian sleep 99999
    ```
- 🌞 vérifier le réseau du conteneur
  -   vérifier que le conteneur a bien une carte réseau et repérer son IP
      -   c'est une des interfaces de la _veth pair_
  -   possible avec un shell dans le conteneur OU avec un `docker inspect` depuis l'hôte
    ```bash
    [jdinh@localhost ~]$ sudo docker exec -it 54b92733be4 bash
    root@54b92733be48:/# ip a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
        inet 127.0.0.1/8 scope host lo
           valid_lft forever preferred_lft forever
    5: eth0@if6: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
        link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
        inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
           valid_lft forever preferred_lft forever
    ```

- 🌞 vérifier le réseau sur l'hôte
  -   vérifier qu'il existe une première carte réseau qui porte une IP dans le même réseau que le conteneur
    ```bash
    [jdinh@localhost ~]$ ip a
    [...]
    4: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
        link/ether 02:42:e7:67:45:ab brd ff:ff:ff:ff:ff:ff
        inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
           valid_lft forever preferred_lft forever
        inet6 fe80::42:e7ff:fe67:45ab/64 scope link
           valid_lft forever preferred_lft forever
    [...]
    ```
  -   vérifier qu'il existe une deuxième carte réseau, qui est la deuxième interface de la _veth pair_
      -   son nom ressemble à _vethXXXXX@ifXX_
    ```bash
    [jdinh@localhost ~]$ ip a
    [...]
    6: vethb1cf337@if5: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default
        link/ether 02:f1:57:29:2c:09 brd ff:ff:ff:ff:ff:ff link-netnsid 0
        inet6 fe80::f1:57ff:fe29:2c09/64 scope link
           valid_lft forever preferred_lft forever
    ```
  -   identifier les règles _iptables_ liées à la création de votre conteneur
```bash
[jdinh@localhost ~]$ sudo iptables -vnL
[...]
Chain DOCKER (1 references)
 pkts bytes target     prot opt in     out     source               destination
    0     0 ACCEPT     tcp  --  !docker0 docker0  0.0.0.0/0            172.17.0.2           tcp dpt:7777

Chain DOCKER-ISOLATION-STAGE-1 (1 references)
 pkts bytes target     prot opt in     out     source               destination
    0     0 DOCKER-ISOLATION-STAGE-2  all  --  docker0 !docker0  0.0.0.0/0            0.0.0.0/0
    0     0 RETURN     all  --  *      *       0.0.0.0/0            0.0.0.0/0

Chain DOCKER-ISOLATION-STAGE-2 (1 references)
 pkts bytes target     prot opt in     out     source               destination
    0     0 DROP       all  --  *      docker0  0.0.0.0/0            0.0.0.0/0
    0     0 RETURN     all  --  *      *       0.0.0.0/0            0.0.0.0/0

Chain DOCKER-USER (1 references)
 pkts bytes target     prot opt in     out     source               destination
    0     0 RETURN     all  --  *      *       0.0.0.0/0            0.0.0.0/0

[...]
```
### 2. Cgroups
#### A. Découverte manuelle

- 🌞 Lancer un conteneur Docker et déduire dans quel cgroup il s'exécute
```
[jdinh@localhost ~]$ sudo systemd-cgtop
Path                                                                                                                                                                    Tasks   %CPU   Memory  Input/s Output/s

/                                                                                                                                                                          71      -   675.3M        -        -
/docker                                                                                                                                                                     -      -    80.0K        -        -
/docker/54b92733be4833bf421448098ded4f55c805571b302ede1fe6c26c7adda5fc05                                                                                                    1      -    80.0K        -        -
/system.slice                                                                                                                                                               -      -   489.2M        -        -
/system.slice/NetworkManager.service                                                                                                                                        3      -    14.2M        -        -
/system.slice/auditd.service                                                                                                                                                1      -     2.7M        -        -
/system.slice/containerd.service                                                                                                                                            2      -    81.7M        -        -
/system.slice/crond.service                                                                                                                                                 1      -   748.0K        -        -
/system.slice/dbus.service                                                                                                                                                  1      -     1.7M        -        -
/system.slice/dev-hugepages.mount                                                                                                                                           -      -   248.0K        -        -
/system.slice/dev-mapper-centos\x2dswap.swap                                                                                                                                -      -    60.0K        -        -
/system.slice/dev-mqueue.mount                                                                                                                                              -      -   128.0K        -        -
/system.slice/docker.service                                                                                                                                                2      -   284.6M        -        -
/system.slice/firewalld.service                                                                                                                                             1      -    34.1M        -        -
/system.slice/lvm2-lvmetad.service                                                                                                                                          1      -     1.6M        -        -
/system.slice/polkit.service                                                                                                                                                1      -    14.8M        -        -
/system.slice/postfix.service                                                                                                                                               3      -     6.3M        -        -
/system.slice/rsyslog.service                                                                                                                                               1      -     2.7M        -        -
/system.slice/sshd.service                                                                                                                                                  1      -     6.8M        -        -
/system.slice/sys-kernel-debug.mount                                                                                                                                        -      -   388.0K        -        -
/system.slice/system-getty.slice                                                                                                                                            1      -   184.0K        -        -
/system.slice/system-getty.slice/getty@tty1.service                                                                                                                         1      -        -        -        -
/system.slice/system-lvm2\x2dpvscan.slice                                                                                                                                   -      -     4.0K        -        -
/system.slice/systemd-journald.service                                                                                                                                      1      -     1.2M        -        -
/system.slice/systemd-logind.service                                                                                                                                        1      -   952.0K        -        -
/system.slice/systemd-udevd.service                                                                                                                                         1      -    11.4M        -        -
/system.slice/tuned.service                                                                                                                                                 1      -    13.1M        -        -
/user.slice                                                                                                                                                                 9      -   160.2M        -        -
/user.slice/user-0.slice/session-3.scope                                                                                                                                    1      -        -        -        -
/user.slice/user-1000.slice/session-1.scope                                                                                                                                 5      -        -        -        -
/user.slice/user-1000.slice/session-2.scope                                                                                                                                 3      -        -        -        -
```

Il s'exécute dans le cgroup `/docker/54b92733be4833bf421448098ded4f55c805571b302ede1fe6c26c7adda5fc05`

#### B. Utilisation par Docker

🌞 Lancer un conteneur Docker et trouver

-   la mémoire RAM max qui lui est autorisée
```bash
[jdinh@localhost ~]$ cat /sys/fs/cgroup/memory/docker/54b92733be4833bf421448098ded4f55c805571b302ede1fe6c26c7adda5fc05/memory.limit_in_bytes
9223372036854771712
```
-   le nombre de processus qu'il peut contenir
```bash
[jdinh@localhost ~]$ cat /sys/fs/cgroup/cpu/docker/54b92733be4833bf421448098ded4f55c805571b302ede1fe6c26c7adda5fc05/cpu.shares
1024
```

🌞 Altérer les valeurs cgroups allouées par défaut avec des options de la commandes `docker run` (au moins 3)

-   préciser les options utilisées
-   prouver en regardant dans `/sys` qu'elles sont utilisées
```bash
[jdinh@localhost ~]$ sudo docker run -d -p 5555:6666 --cpu-shares 50 alpine sleep 99999
0a10d9816abf8b7e32331eb262a19b1c5f61bc74060ccd2795cccda2e6e83e84
[jdinh@localhost ~]$ cat /sys/fs/cgroup/cpu/docker/0a10d9816abf8b7e32331eb262a19b1c5f61bc74060ccd2795cccda2e6e83e84/cpu.shares
50

[jdinh@localhost ~]$ sudo docker run -d -p 5555:6666 --memory 1024000000 alpine sleep 99999
fb9e642b3059c7b4abd09d1062fd429a8e3622d1439f9bf4b87360c0aaf2332f
[jdinh@localhost ~]$ cat /sys/fs/cgroup/memory/docker/fb9e642b3059c7b4abd09d1062fd429a8e3622d1439f9bf4b87360c0aaf2332f/memory.limit_in_bytes
1024000000
```

### 3. Capabilities

#### A. Découverte manuelle

- 🌞 déterminer les capabilities actuellement utilisées par votre shell
```bash
[jdinh@localhost ~]$ capsh --print
Current: =
Bounding set =cap_chown,cap_dac_override,cap_dac_read_search,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_linux_immutable,cap_net_bind_service,cap_net_broadcast,cap_net_admin,cap_net_raw,cap_ipc_lock,cap_ipc_owner,cap_sys_module,cap_sys_rawio,cap_sys_chroot,cap_sys_ptrace,cap_sys_pacct,cap_sys_admin,cap_sys_boot,cap_sys_nice,cap_sys_resource,cap_sys_time,cap_sys_tty_config,cap_mknod,cap_lease,cap_audit_write,cap_audit_control,cap_setfcap,cap_mac_override,cap_mac_admin,cap_syslog,35,36
Securebits: 00/0x0/1'b0
 secure-noroot: no (unlocked)
 secure-no-suid-fixup: no (unlocked)
 secure-keep-caps: no (unlocked)
uid=1000(jdinh)
gid=1000(jdinh)
groups=10(wheel),1000(jdinh)
```

- 🌞 Déterminer les capabilities du processus lancé par un conteneur Docker
```bash
[jdinh@localhost ~]$ cat /proc/2797/status | grep Cap
CapInh: 0000000000000000
CapPrm: 0000001fffffffff
CapEff: 0000001fffffffff
CapBnd: 0000001fffffffff
CapAmb: 0000000000000000
```

- 🌞 Jouer avec `ping`
    -   trouver le chemin absolu de `ping`
    ```bash
    /usr/bin/ping
    ```
    -   récupérer la liste de ses capabilities
    ```bash
    [jdinh@localhost ~]$ getcap /usr/bin/ping
    /usr/bin/ping = cap_net_admin,cap_net_raw+p
    ```
    -   enlever toutes les capabilities
        -   en utilisant une liste vide
        -   `setcap '' <PATH>`
    ```bash
    [jdinh@localhost ~]$ sudo setcap '' /usr/bin/ping
    ```
    -   vérifier que `ping` ne fonctionne plus
    ```bash
    [jdinh@localhost ~]$ ping 127.0.0.1
    ping: socket: Operation not permitted
    ```
    -   vérifier avec `strace` que c'est bien l'accès à l'ICMP qui a été enlevé
    ```bash
    [jdinh@localhost ~]$ strace ping 127.0.0.1
    [...]
    socket(AF_INET, SOCK_DGRAM, IPPROTO_ICMP) = -1 EACCES (Permission denied)
    socket(AF_INET, SOCK_RAW, IPPROTO_ICMP) = -1 EPERM (Operation not permitted)
    [...]
    ```
#### B. Utilisation par Docker

🌞 lancer un conteneur NGINX qui a le strict nécessaire de capabilities pour fonctionner

-   prouver qu'il fonctionne
```bash
[jdinh@localhost ~]$ sudo docker run -p 80:80 --cap-drop all --cap-add=chown --cap-add=net_bind_service --cap-add=setuid --cap-add=setgid -d nginx
[jdinh@localhost ~]$ sudo docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
46f7dd0daf30        nginx               "nginx -g 'daemon of…"   42 seconds ago      Up 41 seconds       0.0.0.0:80->80/tcp       reverent_chatelet
```
-   expliquer toutes les capabilities dont il a besoin

Il a besoin de pouvoir changer l'appartenance à des fichiers. D'où le rajout de la `CAP_CHOWN`
```bash
2020/01/27 13:13:14 [emerg] 1#1: chown("/var/cache/nginx/client_temp", 101) failed (1: Operation not permitted)
nginx: [emerg] chown("/var/cache/nginx/client_temp", 101) failed (1: Operation not permitted)
```

Pour pourvoir `bind` le port, il faut `CAP_NET_BIND_SERVICE`
```bash
2020/01/27 13:15:37 [emerg] 1#1: bind() to 0.0.0.0:80 failed (13: Permission denied)
nginx: [emerg] bind() to 0.0.0.0:80 failed (13: Permission denied)
```

Enfin pour manipuler les process il a besoin de `CAP_SETUID` et `CAP_SETGID`
```
2020/01/27 13:24:06 [emerg] 6#6: setgid(101) failed (1: Operation not permitted)
2020/01/27 13:24:06 [alert] 1#1: worker process 6 exited with fatal code 2 and cannot be respawned
```