# TP 1 - Prise en main de Docker
Auteur : Jonathan DINH<br/>
Classe : B3B<br/>
Date de création : 09/12/2019

## I. Prise en main
### 1. Lancer des conteneurs
- lister les conteneurs actifs, et mettre en évidence le conteneur lancé sur le `sleep`
	-   trouver son nom et ID
	```
	[root@localhost ~]# docker ps
	CONTAINER ID        IMAGE               COMMAND             CREATED              STATUS              	PORTS               NAMES
	334cd4b01f36        alpine              "sleep 9999"        About a minute ago   Up About a minute                       eager_maxwell
	```
-   Mise en évidence d'une partie de l'isolation mise en place par le conteneur. Montrer que le conteneur utilise :
    -   une arborescence de processus différente
    ```bash
    # sur l'hôte
    [root@localhost ~]# ps -ef | head
    UID        PID  PPID  C STIME TTY          TIME CMD
    root         1     0  0 09:59 ?        00:00:03 /usr/lib/systemd/systemd --system --deserialize 31
    root         2     0  0 09:59 ?        00:00:00 [kthreadd]
    root         3     2  0 09:59 ?        00:00:00 [ksoftirqd/0]
    root         5     2  0 09:59 ?        00:00:00 [kworker/0:0H]
    root         6     2  0 09:59 ?        00:00:01 [kworker/u2:0]
    root         7     2  0 09:59 ?        00:00:00 [migration/0]
    root         8     2  0 09:59 ?        00:00:00 [rcu_bh]
    root         9     2  0 09:59 ?        00:00:01 [rcu_sched]
    root        10     2  0 09:59 ?        00:00:00 [lru-add-drain]
    
    # dans le conteneur
    / # ps -ef
    PID   USER     TIME  COMMAND
    1 root      0:00 sleep 9999
    11 root      0:00 sh
    16 root      0:00 ps -ef
    ```
    -   des cartes réseau différentes
    ```bash
    # sur l'hôte
    [root@localhost ~]# ip a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
    2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:be:0d:e8 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
       valid_lft 81287sec preferred_lft 81287sec
    inet6 fe80::2b83:fc54:3571:2f6f/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
    3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:43:6f:a0 brd ff:ff:ff:ff:ff:ff
    inet 192.168.238.3/24 brd 192.168.238.255 scope global noprefixroute dynamic enp0s8
       valid_lft 1021sec preferred_lft 1021sec
    inet6 fe80::3516:5ce1:4d6f:f0e5/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
    4: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
    link/ether 02:42:d7:86:8c:9e brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:d7ff:fe86:8c9e/64 scope link
       valid_lft forever preferred_lft forever
    10: vethc02daa4@if9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default
    link/ether e6:6f:23:c1:e8:de brd ff:ff:ff:ff:ff:ff link-netnsid 1
    inet6 fe80::e46f:23ff:fec1:e8de/64 scope link
       valid_lft forever preferred_lft forever

    # dans le conteneur
    / # ip a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    9: eth0@if10: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP
    link/ether 02:42:ac:11:00:03 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.3/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever

    ```
    -   des utilisateurs système différents
    ```bash
    # sur l'hôte
    [root@localhost ~]# cat /etc/passwd
    root:x:0:0:root:/root:/bin/bash
    bin:x:1:1:bin:/bin:/sbin/nologin
    daemon:x:2:2:daemon:/sbin:/sbin/nologin
    adm:x:3:4:adm:/var/adm:/sbin/nologin
    lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
    sync:x:5:0:sync:/sbin:/bin/sync
    shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
    halt:x:7:0:halt:/sbin:/sbin/halt
    mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
    operator:x:11:0:operator:/root:/sbin/nologin
    games:x:12:100:games:/usr/games:/sbin/nologin
    ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
    nobody:x:99:99:Nobody:/:/sbin/nologin
    systemd-network:x:192:192:systemd Network Management:/:/sbin/nologin
    dbus:x:81:81:System message bus:/:/sbin/nologin
    polkitd:x:999:997:User for polkitd:/:/sbin/nologin
    postfix:x:89:89::/var/spool/postfix:/sbin/nologin
    sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin
    jdinh:x:1000:1000:Jonathan DINH:/home/jdinh:/bin/bash
    tcpdump:x:72:72::/:/sbin/nologin
    
    # dans le conteneur
    / # cat /etc/passwd
    root:x:0:0:root:/root:/bin/ash
    bin:x:1:1:bin:/bin:/sbin/nologin
    daemon:x:2:2:daemon:/sbin:/sbin/nologin
    adm:x:3:4:adm:/var/adm:/sbin/nologin
    lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
    sync:x:5:0:sync:/sbin:/bin/sync
    shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
    halt:x:7:0:halt:/sbin:/sbin/halt
    mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
    news:x:9:13:news:/usr/lib/news:/sbin/nologin
    uucp:x:10:14:uucp:/var/spool/uucppublic:/sbin/nologin
    operator:x:11:0:operator:/root:/sbin/nologin
    man:x:13:15:man:/usr/man:/sbin/nologin
    postmaster:x:14:12:postmaster:/var/spool/mail:/sbin/nologin
    cron:x:16:16:cron:/var/spool/cron:/sbin/nologin
    ftp:x:21:21::/var/lib/ftp:/sbin/nologin
    sshd:x:22:22:sshd:/dev/null:/sbin/nologin
    at:x:25:25:at:/var/spool/cron/atjobs:/sbin/nologin
    squid:x:31:31:Squid:/var/cache/squid:/sbin/nologin
    xfs:x:33:33:X Font Server:/etc/X11/fs:/sbin/nologin
    games:x:35:35:games:/usr/games:/sbin/nologin
    postgres:x:70:70::/var/lib/postgresql:/bin/sh
    cyrus:x:85:12::/usr/cyrus:/sbin/nologin
    vpopmail:x:89:89::/var/vpopmail:/sbin/nologin
    ntp:x:123:123:NTP:/var/empty:/sbin/nologin
    smmsp:x:209:209:smmsp:/var/spool/mqueue:/sbin/nologin
    guest:x:405:100:guest:/dev/null:/sbin/nologin
    nobody:x:65534:65534:nobody:/:/sbin/nologin
    ```
    -   des points de montage (les partitions montées) différents
    ```bash
    # sur l'hôte
    [root@localhost ~]# df -h
    Filesystem               Size  Used Avail Use% Mounted on
    devtmpfs                 484M     0  484M   0% /dev
    tmpfs                    496M     0  496M   0% /dev/shm
    tmpfs                    496M  7.0M  489M   2% /run
    tmpfs                    496M     0  496M   0% /sys/fs/cgroup
    /dev/mapper/centos-root  6.2G  1.9G  4.4G  31% /
    /dev/sda1               1014M  186M  829M  19% /boot
    tmpfs                    100M     0  100M   0% /run/user/0
    overlay                  6.2G  1.9G  4.4G  31% /var/lib/docker/overlay2/dab56383d43419697fadf25f9fb27465069eb25701c552c444ebd9c23c7740be/merged

    # dans le conteneur
    / # df -h
    Filesystem                Size      Used Available Use% Mounted on
    overlay                   6.2G      1.9G      4.3G  30% /
    tmpfs                    64.0M         0     64.0M   0% /dev
    tmpfs                   495.6M         0    495.6M   0% /sys/fs/cgroup
    shm                      64.0M         0     64.0M   0% /dev/shm
    /dev/mapper/centos-root
                          6.2G      1.9G      4.3G  30% /etc/resolv.conf
    /dev/mapper/centos-root
                          6.2G      1.9G      4.3G  30% /etc/hostname
    /dev/mapper/centos-root
                          6.2G      1.9G      4.3G  30% /etc/hosts
    tmpfs                   495.6M         0    495.6M   0% /proc/asound
    tmpfs                   495.6M         0    495.6M   0% /proc/acpi
    tmpfs                    64.0M         0     64.0M   0% /proc/kcore
    tmpfs                    64.0M         0     64.0M   0% /proc/keys
    tmpfs                    64.0M         0     64.0M   0% /proc/timer_list
    tmpfs                    64.0M         0     64.0M   0% /proc/timer_stats
    tmpfs                    64.0M         0     64.0M   0% /proc/sched_debug
    tmpfs                   495.6M         0    495.6M   0% /proc/scsi
    tmpfs                   495.6M         0    495.6M   0% /sys/firmware
    ```
-    détruire le conteneur avec `docker rm`
```bash
[root@localhost ~]# docker stop 334cd4b01f36
334cd4b01f36
[root@localhost ~]# docker rm 334cd4b01f36
334cd4b01f36
```
**Lancer un conteneur NGINX**

-   utiliser l'image `nginx`
-   lancer le conteneur en démon
-   utiliser `-p` pour partager un port de l'hôte vers le port `80` du conteneur
-   visiter le service web en utilisant l'IP de l'hôte (= en utilisant votre partage de port)
```bash
[root@localhost ~]# docker run -p 80:80 -d nginx
1a2fa174446258023adc322c1e3f1bc586eb22071d8b577b166ea9774348d2bd
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                NAMES
1a2fa1744462        nginx               "nginx -g 'daemon of…"   5 seconds ago       Up 4 seconds        0.0.0.0:80->80/tcp   awesome_cori
```
### 2. Gestion d'images
**Gestion d'images**
-   🌞 récupérer une image de Apache en version 2.2
    -   la lancer en partageant un port qui permet d'accéder à la page d'accueil d'Apache
    ```bash
    [root@localhost ~]# docker pull httpd:2.2
    2.2: Pulling from library/httpd
    f49cf87b52c1: Pull complete
    24b1e09cbcb7: Pull complete
    8a4e0d64e915: Pull complete
    bcbe0eb4ca51: Pull complete
    16e370c15d38: Pull complete
    Digest: sha256:9784d70c8ea466fabd52b0bc8cde84980324f9612380d22fbad2151df9a430eb
    Status: Downloaded newer image for httpd:2.2
    docker.io/library/httpd:2.2

    [root@localhost ~]# docker run -d -p 80:80 httpd:2.2
    ```

**Création d'image**

-   🌞 créer une image qui lance un serveur web python. L'image doit :
    -   se baser sur alpine
    -   contenir `python3`
    -   utiliser la clause `EXPOSE` afin d'être plus explicite
    -   contenir une clause `WORKDIR` afin de spécifier un répertoire de travail (les commandes suivantes seront lancées depuis ce répertoire)
    -   utiliser `COPY` pour récupérer un fichier à partager à l'aide du serveur HTTP
    -   lancer la commande `python3 -m http.server 8888` pour lancer un serveur web qui écoute sur le port TCP 8888 de toutes les interfaces du conteneur
    Le `Dockerfile` :
    ```
    FROM alpine:latest

    RUN apk update && apk add python3

    WORKDIR ./app

    COPY index.html .

    CMD ["python3", "-m", "http.server", "8888"]

    EXPOSE 8888
    ```
    Ensuite :
    ```
    [root@localhost ~]# docker build .
    ```
**expliquer comment**
-   lancer le conteneur et accéder au fichier partagé depuis l'hôte (en accédant au serveur web)
    -   avec un `docker run` et les bonnes options
    ```bash
    [root@localhost ~]# docker run -p 8888:8888 -d 10b1edfefe96
    ```
-   utiliser l'option `-v` de `docker run` pour partager des fichiers de l'hôte à l'aide du serveur web du conteneur
    -   c'est un **volume Docker**
    -   `docker run -v <PATH_ON_HOST>:<PATH_IN_CT>` permet de partager le contenu d'un dossier de l'hôte dans le conteneur
    ```bash
    [root@localhost ~]# echo blabla > ./app/toto.html
    [root@localhost ~]# ll ./app
    total 4
    -rw-r--r--. 1 root root 7 Dec  9 14:17 toto.html
    [root@localhost ~]# docker run -p 8888:8888 -d -v "/root/app:/app" 10b1edfefe96
    [root@localhost ~]# docker exec -it 689217e0d7a8 sh
    /app # ls
    toto.html
    ```
### 3. Manipulation du démon docker

-   modifier le socket utilisé pour la communication avec le démon Docker
    -   trouvez le path du socket UNIX utilisé par défaut :
    `/var/run/docker.sock`
    -   utiliser un socket TCP (port TCP) à la place
    ```bash
    # sur node1
    [root@localhost ~]# dockerd -H tcp://192.168.238.3
    [root@localhost ~]# firewall-cmd --add-port=2375/tcp

    # sur node2
    [root@localhost ~]# docker -H tcp://192.168.238.3:2375 ps -a
    CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
    689217e0d7a8        10b1edfefe96        "python3 -m http.ser…"   53 minutes ago      Exited (137) 32 minutes ago                       recursing_torvalds
    ```
-   modifier l'emplacement des données Docker
    -   trouver l'emplacement par défaut
    `/var/lib/docker`
    -   le déplacer dans un répertoire `/data/docker` créé à cet effet
    Dans le fichier `/etc/docker/daemon.json` :
    ```json
    {
    "data-root": "/data/docker"
    }
    ```
    Puis sur `node2` :
    ```
    [root@localhost ~]# docker -H tcp://192.168.238.3:2375 run -p 8888:8888 -d 8f539bde0901
    [root@localhost ~]# docker -H tcp://192.168.238.3:2375 ps
    CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
    af9a7d3a1038        8f539bde0901        "python3 -m http.ser…"   24 seconds ago      Up 25 seconds       0.0.0.0:8888->8888/tcp   blissful_ishizaka
    ```
    -   modifier le OOM score du démon Docker
    Dans le fichier `/etc/docker/daemon.json` :
    ```json
    {
    "data-root": "/data/docker",
    "oom-score-adjust": -100
    }
    ```
## II. `docker-compose`

**Write your own** (cf. write_your_own pour le résultat attendu)

Ecrire un `docker-compose-v1.yml` qui permet de :
```yml
version: '3.7'

services:
  node:
    build: .
    restart: on-failure
    ports:
      - "8888:8888"
```
Ajouter un deuxième conteneur `docker-compose-v2.yml` :
```yml
version: '3.7'

services:
  web:
    build: ./webserver/
    restart: on-failure
    networks:
      nginx-net:
        aliases:
          - python-app
    expose:
      - "8888"

  proxy:
    image: nginx
    networks:
      - nginx-net
    volumes:
      - ./nginx/conf:/etc/nginx/conf.d
      - ./nginx/certs:/certs
    depends_on:
      - web
    ports:
      - "443:443"

networks:
  nginx-net:
```

**Conteneuriser une application donnée** (cf. python_app pour le résultat attendu)

Le `docker-compose.yml` :

```yml
version: '3.7'

services:
  web:
    build: ./webserver/
    restart: on-failure
    networks:
      internal:
        aliases:
          - python-app
    depends_on:
      - redis

  nginx:
    image: nginx
    networks:
      - internal
    volumes:
      - ./nginx/conf.d:/etc/nginx/conf.d
      - ./nginx/certs:/certs
    depends_on:
      - web
      - redis
    ports:
      - "443:443"

  redis:
    image: redis
    networks:
      internal:
        aliases:
          - db
    expose:
      - "6379"

networks:
  internal:
```

