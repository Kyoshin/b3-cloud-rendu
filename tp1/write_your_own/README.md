# docker-compose-v2.yml

## Installation

Pour démarrer :

```
docker-compose -f docker-compose-v2.yml up -d
```

## Webserver

Il s'agit d'un conteneur alpine qui s'exécute un mini server weben python sur le port 8888. Dans le docker-compose, on build l'image et on l'ajoute au réseau `nginx-net` avec l'aliase `python-app`.

Le `Dockerfile` :

```
FROM alpine:latest

RUN apk update && apk add python3

WORKDIR ./app

COPY index.html .

CMD ["python3", "-m", "http.server", "8888"]

EXPOSE 8888
```

## Nginx

Nginx fait office de reverse proxy ici. Pour le configurer, on modifie le fichier `nginx.conf` et on crée les certificats. Dans le docker-compose, on récupère une image nginx, on lui map les 2 volumes, l'un pour les certificats et l'autre pour la configuration.

Le `nginx.conf` :

```
server {
    server_name   test.docker;
    listen        443 ssl;

    ssl_certificate           /certs/test.docker.crt;
    ssl_certificate_key       /certs/test.docker.key;

    proxy_set_header        Host $host;
    proxy_set_header        X-Real-IP $remote_addr;
    proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header        X-Forwarded-Proto $scheme;

    location      / {
            proxy_pass http://python-app:8888/;
    }
}
```
